/**
 * Overrides `object.d` included in the D runtime. Used as a way of including some basic runtime
 * functionality inside every file implicitly.
 */
module object;

/**
 * Immutable sequence of dynamic memory.
 * Params:
 *  T = Contained type.
 */
public struct Sequence(T) {
	import kym.array : allocCopy, destroy;

	alias array this;

	/** Subscript alias for the sequence length. */
	alias opDollar = length;

	/** Sequence length without null terminator. */
	immutable (size_t) length;

	/** Sequence memory buffer. */
	immutable (T)[] array;

	/**
	 * Creates a [Sequence] based on the given mutable slice of data.
	 * Params:
	 *  data = Reference mutable slice.
	 */
	this(T[] data) {
		if (__ctfe) {
			// Strings baked into the binary are always null-terminated.
			this.array = cast (immutable (T)[])data;
			this.length = (data.length - 1);
		} else if (data) {
			immutable (bool) nullTerminator = (data[$ - 1] == 0);

			this.array = (() {
				T[] buffer;

				if (nullTerminator) {
					buffer = allocCopy(data);
				} else {
					buffer = allocCopy(data, 1);
					buffer[$ - 1] = 0;
				}

				return cast (immutable (T)[])buffer;
			})();

			this.length = nullTerminator ? (data.length - 1) : data.length;
		}
	}

	/**
	 * Creates a [Sequence] based on the given immutable slice of data.
	 * Params:
	 *  data = Reference immutable slice.
	 */
	this(immutable (T)[] data) {
		// If this is being constructed in CTFE we don't need to worry about threads, nor can we
		// actually copy any memory since this isn't being executed at runtime, so just take the
		// slice directly.
		this.array = (__ctfe) ? data : allocCopy(data);
	}

	this(this) {
		// Ensure every blit has a unique copy of the data. May move to reference counting in the
		// future, if it can be done in a thread-safe manner with no overhead.
		this.array = allocCopy(this.array);
	}

	~this() {
		// Again, do not try to delete this while executing at compile-time.
		if (this.array && !__ctfe) {
			// Immutability cannot just be cast away on a slice, it needs to be re-assigned first.
			T[] mutableArray = cast (T[])this.array;

			destroy(mutableArray);
		}
	}
}

version (D_LP64) {
	/** Type big enough to cover all addressable locations in memory on 64-bit hardware. */
	public alias size_t = ulong;
	
	/** Type for relative memory traversal on 64-bit hardware. */
	public alias ptrdiff_t = long;
} else {
	/** Type big enough to cover all addressable locations in memory on 32-bit hardware. */
	public alias size_t = uint;
	
	/** Type for relative memory traversal on 32-bit hardware. */
	public alias ptrdiff_t = int;
}

/** Type for creating `array`s of immutable 8-bit characters evaluatable at compile time. */
public alias string = immutable (char)[];

/** Type for creating `array`s of immutable 16-bit characters evaluatable at compile time. */
public alias dstring = immutable (dchar)[];

/** Type for creating `array`s of immutable 32-bit characters evaluatable at compile time. */
public alias wstring = immutable (wchar)[];

/** Type for creating [Sequence]s of immutable 8-bit characters dynamically at run time. */
public alias String = Sequence!(char);

/** Type for creating [Sequence]s of immutable 16-bit characters dynamically at run time. */
public alias DString = Sequence!(dchar);

/** Type for creating [Sequence]s of immutable 32-bit characters dynamically at run time. */
public alias WString = Sequence!(wchar);
