/**
 * Provides utilities for the handling and manipulation of the filesystem and other standard I/O
 * devices.
 */
module kym.io;

public {
	import core.stdc.stdio : stderr, stdout, stdin;
}

private {
	import core.stdc.stdio;
	import kym.meta : allSatisfy, isAggregateType, isSlice, isSomeChar;
}

/** Type alias with less ambiguous naming convention for the standard C file blackbox. */
public alias File = FILE;

/** Deduces if a type is capable of being serialized by any of I/O functions. */
public enum isSerializable(T) = (isSomeChar!(T) || __traits(isScalar, T) || isSlice!(T) || __traits(isStaticArray, T));

/** Cursor position of a [File]. */
public enum FilePos {
	beg = SEEK_SET,
	cur = SEEK_CUR,
	end = SEEK_END
}

/**
 * Attempts to close the given file handle.
 * Params: file = File handle pointer.
 * Returns: `True` if the file handle was closed, `false` if not.
 */
public bool close(File* file) {
	return (fclose(file) == 0);
}

/**
 * Checks the process' access to a given location on the filesystem, be it a file or folder, with
 * reference to the requirements specified.
 * Params:
 *  path = Path to filesystem location.
 *  mode = File access mode expression. Specifying `null` checks that the file exists, while giving
 *         explicit access modes like (R)ead, (W)rite, or e(X)ecute check for their respective
 *         privileges. **Note**: on Windows a location will always be "executable" if it is both
 *         readable and writable, as Windows has no concept of executable privileges in the Unix
 *         context.
 */
public bool fsAccess(string path, string mode) {
	version (Posix) {
		import core.sys.posix.unistd : F_OK, R_OK, W_OK, X_OK, access;
	} else version (Windows) {
		import core.sys.windows.windows : _access_s;

		enum F_OK = 0;
		enum W_OK = 2;
		enum R_OK = 4;
	} else {
		static assert (false, __FUNCTION__ ~ " is not supported on this platform.");
	}

	int modeBits = F_OK;

	foreach (char modeSet; mode) {
		switch (((modeSet > 64) && (modeSet < 91)) ? (modeSet + 32) : modeSet) {
			case 'r': modeBits |= R_OK; break;
			case 'w': modeBits |= W_OK; break;
			
			case 'x': {
				version (Posix) {
					modeBits |= X_OK;
				} else version (Windows) {
					modeBits |= R_OK;
					modeBits |= W_OK;
				}

				break;
			}
			
			default: break;
		}
	}

	version (Posix) {
		return (access(path.ptr, modeBits) == 0);
	} else version (Windows) {
		return (_access_s(path.ptr, modeBits) == 0);
	}
}

/**
 * Calculates the size of the file.
 * Params: file = File handle pointer.
 * Returns: File size in bytes.
 */
public ulong getSize(File* file) {
	size_t fileSize;

	fseek(file, 0L, SEEK_END);

	fileSize = ftell(file);

	fseek(file, 0L, SEEK_SET);

	return fileSize;
}

/**
 * Returns: `true` if a file's cursor has reached the end, `false` if not.
 */
public bool isEOF(File* file) {
	return (feof(file) == 0);
}

/**
 * Attempts to open the file at the given location.
 * Params:
 *  filePath = Path to file.
 *  mode = File access mode expression.
 * Returns: File handle pointer, or `null` if the file could not be found or opened.
 */
public File* openFile(string filePath, string mode) {
	return fopen(filePath.ptr, mode.ptr);
}

/**
 * Reads the contents of a file as a byte stream up to the buffer's allocated size, casting the
 * stream's data to match the destination buffer's type.
 * Params:
 *  T = Buffer type.
 *  file = File handle pointer.
 *  outBuffer = Pre-allocated target write buffer.
 */
public void read(T)(File* file, T[] outBuffer) if (isSerializable!(T)) {
	fread(outBuffer.ptr, T.sizeof, outBuffer.length, file);
}

/**
 * Reads the contents of a file as a byte stream up to the specified limit, casting the stream's
 * data to match the destination buffer's type.
 * Params:
 *  T = Buffer type.
 *  file = File handle pointer.
 *  outBuffer = Pre-allocated target write buffer.
 *  limit = Read limit.
 */
public void read(T)(File* file, T[] outBuffer, size_t limit) if (isSerializable!(T)) {
	assert ((limit <= outBuffer.length), "Limit cannot be larger than out buffer.");

	fread(outBuffer.ptr, T.sizeof, limit, file);
}

/**
 * Steps through a file byte-by-byte relative to the [FilePos] argument.
 * Params:
 *  file = File handle pointer.
 *  offset = Relative position in a file to step toward.
 *  pos = Starting location.
 * Returns: [true] if successfully stepped to, [false] if not.
 */
public bool seek(File* file, long offset, FilePos pos) {
	return (fseek(file, offset, (cast(int)pos)) == 0);
}

/**
 * Writes the arguments to the specified file.
 * Params:
 *  Args = Serializable argument types.
 *  file = File handle pointer.
 *  args = Serializable data.
 */
public void write(Args...)(File* file, Args args) if (allSatisfy!(isSerializable, Args)) {
	static foreach (arg; args) {
		static if (isSomeChar!(typeof (arg))) {
			// Single character types.
			fputc((cast (int) arg), file);
		} else static if (isSlice!(typeof (arg)) || __traits(isStaticArray, typeof (arg))) {
			// Array slices
			static if (isSomeChar!(typeof (arg[0]))) {
				// Characters.
				foreach (c; arg) {
					fputc((cast (int)c), file);
				}
			} else {
				// Raw write.
				fwrite((cast (void*)arg.ptr), typeof (arg[0]).sizeof, arg.length, file);
			}
		} else {
			// Invalid parameters.
			static assert (false, ((typeof (arg)).stringof ~ " is not a valid argument type."));
		}
	}

	fflush(file);
}
