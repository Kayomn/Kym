/** Algorithmic utility functions for performing complex evaluation and operations on data. */
module kym.algo;

/**
 * Params:
 *  T = Value type.
 *  value = Input value.
 *  smallest = Smallest allowable value.
 *  largest = Largest allowable value.
 * Returns: The input value if it is in range of the `smallest` and `largest` parameters,
 *          otherwise returning the `smallest` if it is smaller, or the `largest` if it is larger.
 */
public pragma(inline, true) T clamp(T)(T value, T smallest, T largest) {
	return max(smallest, min(value, largest));
}

/**
 * Params:
 *  T = Value type.
 *  a = First value.
 *  b = Second value.
 * Returns: Largest of the two values.
 */
public pragma(inline, true) T max(T)(T a, T b) {
	return ((a > b) ? a : b);
}

/**
 * Params:
 *  T = Value type.
 *  a = First value.
 *  b = Second value.
 * Returns: Smallest of the two values.
 */
public pragma(inline, true) T min(T)(T a, T b) {
	return ((a < b) ? a : b);
}

/**
 * Swaps the values in two variables around.
 * Params:
 *  T = Value type.
 *  a = First value.
 *  b = Second value.
 */
public pragma(inline, true) void swap(T)(ref T a, ref T b) {
	T temp = a;
	a = b;
	b = temp;
}