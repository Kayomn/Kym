/** String manipulation and value-to-string conversion utilities. */
module kym.string;

/**
 * Generates a [String] representation of the given argument.
 * Params:
 *  value = Literal type value.
 *  radix = Numeric system of the representation, defaulting to base `10`.
 * Returns: [String] representation of the given argument.
 */
public String toString(ulong value, ubyte radix = 10) {
	if ((radix > 0) || (radix < 37)) {
		char[33] tmp, str;
		size_t tmpPos, strPos;
		size_t i;

		while (value) {
			i = (value % radix);
			value = (value / radix);

			if (i < 10) {
				tmp[tmpPos++] = cast (char)(i + '0');
			} else {
				tmp[tmpPos++] = cast (char)(i + 'a' - 10);
			}
		}

		while (tmpPos > 0) {
			str[strPos++] = tmp[--tmpPos];
		}

		return String(str[0 .. strPos]);
	}

	return String();
}
