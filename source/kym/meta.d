/**
 * Traits and constraints for performing compile-time function execution, meta-language code
 * generation and template expansion.
 */
module kym.meta;

/**
 * Becomes an alias for a sequence of types.
 * Params: TS = Type arguments.
 */
public template TypeTuple(TS...) {
	alias TypeTuple = TS;
}

/**
 * Becomes the given type without any qualifiers that restrict its mutability.
 * Params: T = Qualified type.
 */
public template Unconst(T) {
	static if (is(T U == immutable U)) {
		alias Unconst = U;
	} else static if (is(T U == inout const U)) {
		alias Unconst = U;
	} else static if (is(T U == inout U)) {
		alias Unconst = U;
	} else static if (is(T U == const U)) {
		alias Unconst = U;
	} else {
		alias Unconst = T;
	}
}

/**
 * Becomes an alias for an unqualified `T`.
 * Params: T = Value type.
 */
public template Unqual(T) {
	version (none) {
		// Error: recursive alias declaration @@@BUG1308@@@
			 static if (is(T U ==     const U)) alias Unqual = Unqual!U;
		else static if (is(T U == immutable U)) alias Unqual = Unqual!U;
		else static if (is(T U ==     inout U)) alias Unqual = Unqual!U;
		else static if (is(T U ==    shared U)) alias Unqual = Unqual!U;
		else                                    alias Unqual =        T;
	} else {
		// workaround
			 static if (is(T U ==          immutable U)) alias Unqual = U;
		else static if (is(T U == shared inout const U)) alias Unqual = U;
		else static if (is(T U == shared inout       U)) alias Unqual = U;
		else static if (is(T U == shared       const U)) alias Unqual = U;
		else static if (is(T U == shared             U)) alias Unqual = U;
		else static if (is(T U ==        inout const U)) alias Unqual = U;
		else static if (is(T U ==        inout       U)) alias Unqual = U;
		else static if (is(T U ==              const U)) alias Unqual = U;
		else                                             alias Unqual = T;
	}
}

/**
 * Becomes an array containing every value in the specified `enum`.
 * Params: E = Enum type.
 */
public template EnumMembers(E) if (is(E == enum)) {
	// Supply the specified identifier to an constant value.
	template WithIdentifier(string ident) {
		static if (ident == "Symbolize") {
			template Symbolize(alias value) {
				enum Symbolize = value;
			}
		} else {
			mixin("template Symbolize(alias " ~ ident ~ ") { alias Symbolize = " ~ ident ~ "; }");
		}
	}

	template EnumSpecificMembers(names...) {
		static if (names.length == 1) {
			alias EnumSpecificMembers = TypeTuple!(WithIdentifier!(names[0]).Symbolize!(__traits(
					getMember, E, names[0])));
		} else static if (names.length > 0) {
			alias EnumSpecificMembers = TypeTuple!(WithIdentifier!(names[0]).Symbolize!(__traits(
					getMember, E, names[0])), EnumSpecificMembers!(names[1 .. ($ / 2)]),
					EnumSpecificMembers!(names[($ / 2) .. $]));
		} else {
			alias EnumSpecificMembers = TypeTuple!();
		}
	}

	alias EnumMembers = EnumSpecificMembers!(__traits(allMembers, E));
}

/**
 * Becomes an alias for a type sequence of members in the given type.
 * Params: T = Value type.
 */
public template Fields(T) {
	static if (is(T == struct) || is(T == union)) {
		alias Fields = typeof(T.tupleof[0 .. ($ - __traits(isNested, T))]);
	} else static if (is(T == class)) {
		alias Fields = typeof(T.tupleof);
	} else {
		alias Fields = TypeTuple!(T);
	}
}

/**
 * Becomes `true` if every item in the argument list passes to the given constraint, or `false` if
 * not.
 * Params:
 *  Constraint = Template constaint.
 *  Args = Type arguments.
 */
public template allSatisfy(alias Constraint, Args...) {
	static foreach (arg; Args) {
		static if (!is (typeof (allSatisfy) == bool) && !Constraint!(arg)) {
			enum allSatisfy = false;
		}
	}

	static if (!is (typeof (allSatisfy) == bool)) {
		enum allSatisfy = true;
	}
}

/**
 * Becomes `true` if any item in the argument list passes to the given constraint, or `false` if
 * not.
 * Params:
 *  Constraint = Template constraint.
 *  Args = Type arguments.
 */
public template anySatisfy(alias Constraint, Args...) {
	static foreach (arg; Args) {
		static if (!is(typeof(anySatisfy) == bool) && Constraint!(arg)) {
			enum anySatisfy = true;
		}
	}

	static if (!is(typeof(anySatisfy) == bool)) {
		enum anySatisfy = false;
	}
}

/**
 * Becomes `true` if the given type performs additional operations when the assignment operator is
 * invoked beyond blitting bits, or `false` if not.
 * Params: S = Value type.
 */
public template hasElaborateAssign(S) {
	static if (__traits(isStaticArray, S) && S.length) {
		enum bool hasElaborateAssign = hasElaborateAssign!(typeof(S.init[0]));
	} else static if (is(S == struct)) {
		enum hasElaborateAssign = (is(typeof(S.init.opAssign(rvalueOf!S))) || is(typeof(S.init
				.opAssign(lvalueOf!S))) || anySatisfy!(.hasElaborateAssign, Fields!(S)));
	} else {
		enum bool hasElaborateAssign = false;
	}
}

/**
 * Becomes `true` if the first type value is assignable to the second, or `false` if not. If no
 * second argument is supplied the constraint will check if the first value can be compared to
 * itself.
 * Params:
 *  Lhs = First type value.
 *  Rhs = Second type value.
 */
public enum isAssignable(Lhs, Rhs = Lhs)  = (__traits(compiles, lvalueOf!(Lhs) = rvalueOf!(Rhs)) &&
		__traits(compiles, lvalueOf!(Lhs) = lvalueOf!(Rhs)));

/**
 * Becomes `true` if the type is some sort of character type, or `false` if not.
 * Params: T = Type.
 */
public enum isSomeChar(T) = ((is (T == char)) || (is (T == dchar)) || (is (T == wchar)));

/**
 * Becomes `true` if the type is some sort of string type, or `false` if not.
 * Params: T = Type.
 */
public enum isSomeString(T) = ((is (T == string)) || (is (T == dstring)) || (is (T == wstring)));

/**
 * Becomes `true` if the type is some sort of aggregate type, or `false` if not.
 * Params: T = Type.
 */
public enum isAggregateType(T) = (is (T == struct) || is (T == union));

/**
 * Becomes `true` if the type is some sort of pointer type, or `false` if not.
 * Params: T = Type.
 */
public enum isPointer(T) = (is (T == P*, P));

/**
 * Becomes `true` if the type is some sort of supported character type, or `false` if not.
 * Params: T = Type.
 */
public enum isSlice(T) = (is (T == A[], A));
