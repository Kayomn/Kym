/** Exposes the C math library functions and more. */
module kym.math;

public {
	import core.stdc.math;
}

/** The ratio of a circle's circumference to its diameter. */
public enum pi = 3.1415926535897932384626433832795028841971693993751058209749445923078164062;

/**
 * Params: val = Input value.
 * Returns: Input value rounded down to nearest integral.
 */
float floor(float val) {
	union Value {
        float f;
        uint i;
    }

	Value value = Value(val);
    immutable (int) exponent = ((value.i >> 23) & 0xff);
    immutable (int) fractionalBits = (127 + (23 - exponent));

    if (fractionalBits > 23) {
        return 0.0;
	}

	if (fractionalBits > 0) {
        value.i &= ~((1U << fractionalBits) - 1);
	}

    return value.f;
}