/**
 * Handles allocation / initialization and deallocation / destruction of single values in dynamic
 * memory.
 */
module kym.memory;

private {
	import kym.meta;
}

/**
 * Allocates a single item in memory, initializing it with either its default value or, provided it
 * is a struct, one of its constructors matching the runtime arguments. Values allocated are
 * **not** garbage collected.
 * Params:
 *  T = Allocation type.
 *  Args = Constructor argument types.
 *  args = Allocation type's constructor arguments (if applicable).
 * Returns: Pointer to allocated data.
 * See: [destroy(T)(ref T*)]
 */
public T* allocNew(T, Args...)(Args args) if ((is (T == struct))) {
	import core.stdc.stdlib : malloc;
	import core.stdc.string : memcpy, memset;

	T* ptr = cast (T*)malloc(T.sizeof);

	static if (is (T == struct)) {
		// Composite values (structs).
		emplace!(T)(ptr, args);
	} else {
		// Regular value (int, float, void*, etc.).
		static if (args.length == 1) {
			(*ptr) = args[0];
		} else {
			(*ptr) = T.init;
		}
	}

	return ptr;
}

/**
 * Deallocates and destructs memory, and sets the pointer to null.
 * Params: ptr = Target address reference.
 * Throws: If the target address is already `null`.
 */
public void destroy(T)(ref T* ptr) {
	import core.stdc.stdlib : free;

	assert (ptr, "Attempt to free a null pointer.");

	static if (__traits(hasMember, T, "__xdtor")) {
		ptr.__xdtor();
	} else if (__traits(hasMember, T, "__dtor")) {
		ptr.__dtor();
	}

	free(ptr);

	ptr = null;
}

/**
 * Default initializes a value on an allocated block of memory without invoking any destructor
 * calls or postblit behavior on the original data that populated it.
 * Params:
 *  T = Chunk value type.
 *  chunk = Memory block address.
 */
public void emplace(T)(T* chunk) @safe pure nothrow {
    emplaceRef!(T)(*chunk);
}

/**
 * Calls the constructor of a struct with the given arguments on an allocated block of memory
 * without invoking any destructor calls or postblit behavior on the original data that populated
 * it.
 * Params:
 *  T = Chunk value type.
 *  Args = Constructor argument types.
 *  chunk = Memory block address.
 *  args = Constructor arguments.
 */
public void emplace(T, Args...)(T* chunk, auto ref Args args) if (is(T == struct) || (Args.length
		== 1)) {
    emplaceRef!T(*chunk, args);
}

/**
 * Implementation detail for emplacing the specified value in a chunk of memory with the given
 * initialization type and arguments, circumventing any destructor calls or postblit behavior on
 * the original piece of memory.
 * Params:
 *  T = Initializable value type.
 *  UT = Target value type.
 *  Args = Initializable value argument types.
 *  chunk = Target emplacement memory.
 *  args = Initializable value arguments.
 */
private void emplaceRef(T, UT, Args...)(ref UT chunk, auto ref Args args) {
	static if (args.length == 0) {
		static assert(is(typeof({static T i;})), "Cannot emplace a " ~ T.stringof ~ " because "
				~ T.stringof ~ ".this() is annotated with @disable.");

		emplaceInitializer(chunk);
	} else static if ((!is(T == struct) && (Args.length == 1)) || (Args.length == 1) && is(typeof(
				{T t = args[0];})) || is(typeof(T(args)))) {

		static struct S {
			T payload;

			this(ref Args x) {
				static if (Args.length == 1) {
					static if (is(typeof(payload = x[0]))) {
						payload = x[0];
					} else {
						payload = T(x[0]);
					}
				} else {
					payload = T(x);
				}
			}
		}

		if (__ctfe) {
			static if (is(typeof(chunk = T(args)))) {
				chunk = T(args);
			} else static if (args.length == 1 && is(typeof(chunk = args[0]))) {
				chunk = args[0];
			} else {
				assert(false, ("CTFE emplace doesn't support " ~ T.stringof ~ " from "
						~ Args.stringof));
			}
		} else {
			S* p = () @trusted { return cast(S*)&chunk; }();

			static if (UT.sizeof > 0) {
				emplaceInitializer(*p);
			}

			p.__ctor(args);
		}
	} else static if (is(typeof(chunk.__ctor(args)))) {
		// This catches the rare case of local types that keep a frame pointer
		emplaceInitializer(chunk);
		
		chunk.__ctor(args);
	} else {
		// We can't emplace. Try to diagnose a disabled postblit.
		static assert(!(Args.length == 1 && is(Args[0] : T)), ("Cannot emplace a " ~ T.stringof
				~ " because " ~ T.stringof ~ ".this(this) is annotated with @disable."));

		// We can't emplace.
		static assert(false, (T.stringof ~ " cannot be emplaced from " ~ Args[].stringof ~ '.'));
	}
}

/**
 * Implementation detail for emplacing the specified value in a chunk memory with
 * the given arguments, circumventing any destructor calls or postblit behavior on the original
 * piece of memory.
 * Params:
 *  UT = Target value type.
 *  Args = Initializable value argument types.
 *  chunk = Target emplacement memory.
 *  args = Initializable value arguments.
 * See: [emplaceRef(UT, Args...)(ref UT, auto ref Args)].
 */
private void emplaceRef(UT, Args...)(ref UT chunk, auto ref Args args) if (is(UT == Unqual!(UT))) {
	emplaceRef!(UT, UT)(chunk, args);
}

/**
 * Implementation detail for initializing data at an emplaced memory chunk.
 * Params:
 *  T = Target emplaced type.
 *  chunk = Target emplaced memory.
 */
private void emplaceInitializer(T)(scope ref T chunk) @trusted pure nothrow {
	static if (!hasElaborateAssign!(T) && isAssignable!(T)) {
		// Basically for any non-structs.
		chunk = T.init;
	} else {
		static if (__traits(isZeroInit, T)) {
			import core.stdc.string : memset;

			// Fast.
			memset(&chunk, 0, T.sizeof);
		} else {
			import core.stdc.string : memcpy;

			// Not so fast.
			static immutable (T) init = T.init;

			memcpy(&chunk, &init, T.sizeof);
		}
	}
}
