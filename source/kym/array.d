/**
 * Handles allocation / initialization and deallocation / destruction of multiple value slices in
 * dynamic memory. Memory buffers are wrapped in slices, giving them implicit length and type
 * safety, enabling the compiler to be able to tell the difference between a pointer and an array.
 */
module kym.array;

private {
	import core.stdc.stdlib : malloc;
}

/**
 * Allocates memory for multiple items, initializing them with their default values. Values
 * allocated are **not** garbage collected.
 * Params:
 *  T = Array elements type.
 *  n = Number of elements to allocate for.
 * Returns: Allocated memory array, or `null` if the given array size is `0`.
 * See: [destroy(T)(ref T[])]
 */
public T[] allocArray(T)(size_t n) {
	return allocArray!(T)(n, T.init);
}

/**
 * Allocates memory for multiple items, initializing them with the given reference data. Values
 * allocated are **not** garbage collected.
 * Params:
 *  T = Array elements type.
 *  n = Number of elements to allocate for.
 *  initializer = Initialization reference data.
 * Returns: Allocated memory array, or `null` if the given array size is `0`.
 * See: [destroy(T)(ref T[])]
 */
public T[] allocArray(T)(size_t n, T initializer) {
	if (n) {
		T[] array = (cast (T*)malloc(T.sizeof * n))[0 .. n];
		array[] = initializer;

		return array;
	}

	return null;
}

/**
 * Allocates an array large enough for the provided arguments and initializes them to the argument
 * values. Values allocated are **not** garbage collected.
 * Params:
 *  T = Array elements type.
 *  n = Elements to allocate for and initialize to.
 * Returns: Returns: Allocated memory array, or `null` if no elements were given.
 * See: [destroy(T)(ref T[])]
 */
public T[] allocArrayOf(T)(T[] elements...) {
	if (elements) {
		T[] array = (cast (T*)malloc(T.sizeof))[0 .. elements.length];

		foreach (i; 0 .. elements.length) {
			array[i] = elements[i];
		}

		return array;
	}

	return null;
}

/**
 * Compile-time initializes a static array of anonymous length.
 * Params:
 *  T = Array elements type.
 *  n = Array length.
 *  elements = Array of elements.
 * Returns: Array of elements.
 */
pragma(inline, true) T[n] arrayOf(T, size_t n)(auto ref T[n] elements) {
    return elements;
}

/**
 * Duplicates the given buffer as a new dynamic array allocation.. Values allocated are **not**
 * garbage collected.
 * Params:
 *  T = Array elements type.
 *  arr = Reference slice.
 *  padding = Additional element padding on the end of the new array, defaulting to `0`.
 * Returns: Allocated memory array, or `null` if the given array size is `0` / is `null`.
 */
public T[] allocCopy(T)(T[] arr, size_t padding = 0) {
	import kym.meta : Unconst;
	import core.stdc.string : memcpy;

	if (arr.length) {
		alias MutableT = Unconst!(T);
		immutable (size_t) byteWidth = (T.sizeof * (arr.length + padding));
		MutableT* copy = cast (MutableT*)malloc(byteWidth);

		memcpy(copy, arr.ptr, byteWidth);

		return cast (T[])copy[0 .. arr.length];
	}

	return null;
}

/**
 * Deallocates and destructs memory, and sets the array to `null`.
 * setting the array to `null`.
 * Params:
 *  array = Target array reference.
 * Throws: If the target array is already `null`.
 */
public void destroy(T)(ref T[] array) {
	import core.stdc.stdlib : free;

	assert (array, "Attempt to free a null array.");

	static if (__traits(hasMember, T, "__xdtor")) {
		// Arrays containing structs need their mixin destructors calling.
		for (size_t i; i < array.length; i++) {
			array[i].__xdtor();
		}
	} else static if (__traits(hasMember, T, "__dtor")) {
		// Arrays containing structs need their destructors calling.
		for (size_t i; i < array.length; i++) {
			array[i].__dtor();
		}
	}

	free(array.ptr);

	array = null;
}
